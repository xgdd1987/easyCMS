<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use CodeIgniter\CodingStandard\CodeIgniter4;
use Nexus\CsConfig\Factory;
use PhpCsFixer\Finder;

// vendor/bin/php-cs-fixer fix --verbose app
$finder = Finder::create()
    ->files()
    ->in([__DIR__])
    ->exclude(['build'])
    ->append([__FILE__]);

$overrides = [
    'declare_strict_types'  => true,
    'phpdoc_order_by_value' => [
        'annotations' => [
            'author',
            'covers',
            'coversNothing',
            'dataProvider',
            'depends',
            'group',
            'internal',
            'method',
            //            'property',
            //            'property-read',
            //            'property-write',
            'requires',
            'throws',
            'uses',
        ],
    ],
];

$options = [
    'finder'    => $finder,
    'cacheFile' => 'build/.php-cs-fixer.cache',
];

return Factory::create(new CodeIgniter4(), $overrides, $options)->forLibrary(
    'easyCMS',
    'easyCMS',
    'easyCMS@easycms.net.cn',
    2024,
);
