<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\common;

class Util
{
    /**
     * 密码哈希
     *
     * @return false|string|null
     */
    public static function passwordEncode(mixed $password, string $algo = PASSWORD_DEFAULT): bool|string|null
    {
        return password_hash($password, $algo);
    }

    /**
     * 验证密码哈希
     */
    public static function passwordVerify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }

    public static function randomkeys($length): string
    {
        $pattern = '0123456789';
        $key     = '';

        for ($i = 0; $i < $length; $i++) {
            $key .= $pattern[mt_rand(0, 9)];
        }

        return $key;
    }
}
// $u=new Util();
// echo $u->randomkeys(6);
// echo $u->passwordEncode('111111');
