<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\model;

use DateTimeInterface;
use support\Model;

class BaseModel extends Model
{
    public const STATUS_ABNORMAL = 0; // 临时
    public const STATUS_NORMAL   = 1; // 正常
    public const STATUS_DELETE   = -1; // 删除
    public const LOCK_NO         = 0; // 非冻结
    public const LOCK_YES        = 1; // 冻结
    public const DEFAULT_NO      = 0;
    public const DEFAULT_YES     = 1;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * 格式化日期
     */
    protected function serializeDate(DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }
}
