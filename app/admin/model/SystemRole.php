<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\admin\model;

use app\model\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int    $id         (主键)
 * @property string $name
 * @property int    $status
 * @property string $created_at
 * @property string $updated_at
 */
class SystemRole extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_role';

    /**
     * 拥有此角色的用户
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(SystemAdmin::class, 'system_admin_role', 'role_id', 'userid');
    }

    /**
     * 拥有此角色的菜单
     */
    public function menus(): BelongsToMany
    {
        return $this->belongsToMany(SystemMenu::class, 'system_role_right', 'role_id', 'menu_id');
    }
}
