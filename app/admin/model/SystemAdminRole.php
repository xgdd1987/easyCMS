<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\admin\model;

use app\model\BaseModel;

/**
 * @property int    $id         (主键)
 * @property int    $userid
 * @property int    $role_id
 * @property string $created_at
 * @property string $updated_at
 */
class SystemAdminRole extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_admin_role';
}
