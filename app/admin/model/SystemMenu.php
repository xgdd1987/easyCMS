<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\admin\model;

use app\model\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * ey_system_menu 系统菜单表
 *
 * @property int    $id        ID(主键)
 * @property int    $pid       父ID
 * @property string $title     名称
 * @property int    $type      菜单类型
 * @property string $icon      菜单图标
 * @property string $href      链接
 * @property string $target    链接打开方式
 * @property string $permits   权限标识
 * @property int    $level     层级
 * @property int    $sort      菜单排序
 * @property int    $status    状态(0:禁用,1:启用)
 * @property string $remark    备注信息
 * @property string $create_at 创建时间
 * @property string $update_at 更新时间
 */
class SystemMenu extends BaseModel
{
    public const TYPE_MENU   = 1; // 菜单
    public const TYPE_BUTTON = 2; // 按钮

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_menu';

    /**
     * 拥有此菜单的角色
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(SystemRole::class, 'system_role_right', 'menu_id', 'role_id');
    }
}
