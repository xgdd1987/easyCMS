<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\admin\model;

use app\model\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int    $id              (主键)
 * @property string $username
 * @property string $password
 * @property string $nickname
 * @property int    $user_type       1：超级管理员 0：普通管理员
 * @property string $is_lock         0：否 1：冻结
 * @property string $last_login_ip   上次登录ip
 * @property string $last_login_time 上次登录时间
 * @property string $status          -1:冻结 0：未激活 1：正常
 * @property string $created_at
 * @property string $updated_at
 */
class SystemAdmin extends BaseModel
{
    public const SUPER_ADMIN  = 1; // 超级管理员
    public const NORMAL_ADMIN = 0; // 普通管理员

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_admin';

    /**
     * 用户所拥有的角色
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(SystemRole::class, 'system_admin_role', 'userid', 'role_id');
    }
}
