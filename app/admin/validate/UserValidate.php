<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\admin\validate;

use think\Validate;

class UserValidate extends Validate
{
    protected $rule = [
        'roles'    => 'require',
        'username' => 'require|max:20',
        'nickname' => 'require|max:10',
        'password' => 'min:6|max:20',
    ];
    protected $message = [
        'roles.require'    => '请选择角色',
        'username.require' => '请填写用户账号',
        'username.max'     => '用户账号最多不能超过20个字符',
        'nickname.require' => '请填写用户昵称',
        'nickname.max'     => '用户昵称最多不能超过10个字符',
        'password.min'     => '密码至少6个字符',
        'password.max'     => '密码最多20个字符',
    ];
    protected $scene = [
        'edit' => ['roles', 'nickname', 'password'],
    ];
}
