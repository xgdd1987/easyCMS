<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\admin\validate;

use think\Validate;

class RoleValidate extends Validate
{
    protected $rule = [
        'name' => 'require|max:20',
    ];
    protected $message = [
        'name.require' => '名称必须',
        'name.max'     => '名称最多不能超过20个字符',
    ];
    protected $scene = [];
}
