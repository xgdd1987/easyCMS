<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\admin\validate;

use think\Validate;

class ChangePasswordValidate extends Validate
{
    protected $rule = [
        'old_password'   => 'require|max:20',
        'new_password'   => 'require|max:20',
        'again_password' => 'require|max:20|confirm:new_password',
    ];
    protected $message = [
        'old_password.require'   => '原密码不能空',
        'old_password.max'       => '原密码最多不能超过20个字符',
        'new_password.require'   => '新密码不能空',
        'new_password.max'       => '新密码最多不能超过20个字符',
        'again_password.require' => '确认密码不能空',
        'again_password.max'     => '确认密码最多不能超过20个字符',
        'again_password.confirm' => '确认密码与新密码不一致',
    ];
    protected $scene = [];
}
