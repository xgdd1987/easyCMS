<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\admin\validate;

use think\Validate;

class MenuValidate extends Validate
{
    protected $rule = [
        'pid'    => 'require',
        'title'  => 'require|max:10',
        'type'   => 'require',
        'target' => 'require',
        'sort'   => 'require|number',
    ];
    protected $message = [
        'pid.require'    => '请选择上级菜单',
        'title.require'  => '请菜单名称',
        'title.max'      => '名称最多不能超过10个字符',
        'type.require'   => '请选择菜单类型',
        'target.require' => '请选择打开方式',
        'sort.require'   => '请输入排序',
        'sort.number'    => '排序只能为数字',
    ];
    protected $scene = [
        'edit' => ['name', 'age'],
    ];
}
