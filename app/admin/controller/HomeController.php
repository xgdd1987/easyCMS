<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\admin\controller;

use app\admin\model\SystemAdmin;
use app\admin\model\SystemMenu;
use app\admin\validate\ChangePasswordValidate;
use app\common\Util;
use app\model\BaseModel;
use support\Request;

class HomeController
{
    public function index(Request $request): \support\Response
    {
        return view('index');
    }

    /**
     * 获取菜单列表
     */
    public function getMenu(): \support\Response
    {
        $code = 0;
        $msg  = '';

        $admin = admin();
        $list  = [];
        // 暂时先把所有的菜单展示出来，用户在点击菜单的时候做权限判断
        $list = SystemMenu::select(['id', 'pid', 'title', 'href', 'icon', 'target'])->where('type', SystemMenu::TYPE_MENU)->where('status', BaseModel::STATUS_NORMAL)->orderBy('sort', 'ASC')->get()->toArray();

        $menuInfo = $this->recursionHandleData($list);
        $menus    = [
            'homeInfo' => [
                'title' => '首页',
                'href'  => '',
            ],
            'logoInfo' => [
                'title' => 'easyCMS',
                'image' => '/resource/layuimini/images/logo.png',
                'href'  => '',
            ],
            'menuInfo' => $menuInfo,
        ];

        return json($menus);
    }

    /**
     * 将数组转换为父子结构数组
     */
    private function recursionHandleData(array $data, int $pid = 0): array
    {
        $child = [];

        foreach ($data as $value) {
            if ($value['pid'] === $pid) {
                $value['child'] = $this->recursionHandleData($data, $value['id']);
                $child[]        = $value;
            }
        }

        return $child;
    }

    /**
     * 未授权提示页面
     */
    public function noAuth(): \support\Response
    {
        return view('noAuth');
    }

    /**
     * 修改密码
     */
    public function changePassword(): \support\Response
    {
        return view('changePassword');
    }

    /**
     * 修改密码
     */
    public function doChangePassword(Request $request): \support\Response
    {
        $code = 0;
        $msg  = '';

        $postData = [
            'old_password'   => $request->post('old_password'),
            'new_password'   => $request->post('new_password'),
            'again_password' => $request->post('again_password'),
        ];

        $validate = new ChangePasswordValidate();
        if ($validate->check($postData)) {
            $admin = admin();
            $admin = SystemAdmin::find($admin['id']);
            if (Util::passwordVerify($postData['old_password'], $admin->password)) {
                $admin->password = Util::passwordEncode($postData['new_password']);
                if ($admin->save()) {
                    $msg = '修改成功';
                } else {
                    $code = 1;
                    $msg  = '修改失败';
                }
            } else {
                $code = 1;
                $msg  = '原密码不正确';
            }
        } else {
            $code = 1;
            $msg  = $validate->getError();
        }

        return json(['code' => $code, 'msg' => $msg]);
    }

    /**
     * 清理缓存
     */
    public function clearCache(): \support\Response
    {
        $code = 1;
        $msg  = '清除缓存成功';

        return json(['code' => $code, 'msg' => $msg]);
    }
}
