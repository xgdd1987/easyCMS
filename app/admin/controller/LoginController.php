<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\admin\controller;

use app\admin\model\SystemAdmin;
use app\common\Util;
use app\model\BaseModel;
use support\Request;
use Webman\Captcha\CaptchaBuilder;

class LoginController
{
    /**
     * 不需要登录的方法
     *
     * @var list<string>
     */
    protected array $noNeedLogin = ['index', 'doLogin', 'logout', 'captcha'];

    /**
     * 不需要鉴权的方法
     *
     * @var list<string>
     */
    protected array $noNeedAuth = [];

    /**
     * 登录页面
     */
    public function index(Request $request): \support\Response
    {
        return view('login');
    }

    /**
     * 登录判断
     */
    public function doLogin(Request $request): \support\Response
    {
        $code     = 0;
        $msg      = '';
        $data     = [];
        $username = $request->post('username');
        $password = $request->post('password');
        $captcha  = $request->post('captcha');

        $session = $request->session();
        if ($username === null) {
            $code = 1;
            $msg  = '用户名不能为空';
        } elseif ($password === null) {
            $code = 1;
            $msg  = '密码不能为空';
        } elseif ($captcha === null) {
            $code = 1;
            $msg  = '验证码不能为空';
        } elseif (strtolower($captcha) !== $session->get('admin.captcha')) {
            $code = 1;
            $msg  = '输入的验证码不正确';
            $session->set('admin.captcha', '');
        } else {
            $admin = SystemAdmin::where('username', $username)->first();
            if ($admin !== null && Util::passwordVerify($password, $admin->password)) {
                if ($admin->status !== BaseModel::STATUS_NORMAL) {
                    $code = 1;
                    $msg  = '该账号未激活';
                } elseif ($admin->is_lock !== BaseModel::LOCK_NO) {
                    $code = 1;
                    $msg  = '该账号已经被锁定';
                } else {
                    // 登录成功
                    $session->set('admin.id', $admin->id);
                    $session->set('admin', $admin->toArray());
                    $data['url'] = '/admin/home';
                }
            } else {
                $code = 1;
                $msg  = '用户名或者密码错误';
                $session->set('admin.captcha', '');
            }
        }

        return json(['code' => $code, 'msg' => $msg, 'data' => $data]);
    }

    /**
     * 验证码
     */
    public function captcha(Request $request): \support\Response
    {
        $builder = new CaptchaBuilder(4);
        $builder->build();
        $request->session()->set('admin.captcha', strtolower($builder->getPhrase()));
        $img_content = $builder->get();

        return response($img_content, 200, ['Content-Type' => 'image/jpeg']);
    }

    /**
     * 退出登录
     */
    public function logout(Request $request): \support\Response
    {
        $session = $request->session();
        $session->forget('admin');

        return redirect('/admin/login');
    }
}
