<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\easy\model;

use app\model\BaseModel;

/**
 * ey_channel
 *
 * @property int    $id         (主键)
 * @property string $name
 * @property string $table_name
 * @property string $value
 * @property int    $status
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 */
class Channel extends BaseModel
{
    public const TABLE_PREFIX = 'channel_';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'channel';
}
