<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\easy\model;

use app\model\BaseModel;

/**
 * ey_theme
 *
 * @property int    $id          (主键)
 * @property string $name        主题名称
 * @property string $dir_name    所在目录名
 * @property string $desc        说明
 * @property string $pic
 * @property int    $type        0:免费 1：收费
 * @property float  $price       价格
 * @property string $author      作者
 * @property string $author_home 作者主页
 * @property string $version
 * @property int    $is_activate 0:未使用 1:使用中
 * @property int    $status
 * @property string $created_at  创建时间
 * @property string $updated_at  更新时间
 */
class Theme extends BaseModel
{
    public const TEMPLATE_ROOT    = '/resource/themes/';
    public const STATIC_FILE_ROOT = '/public/static/';

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'theme';
}
