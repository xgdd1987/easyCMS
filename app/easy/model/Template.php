<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\easy\model;

use app\model\BaseModel;

/**
 * ey_template
 *
 * @property int    $id         (主键)
 * @property string $name
 * @property string $file_name
 * @property int    $type       1:首页模板 2:封面模板 3:列表模板 4:内容模板 5:标签模板 6:公共模板 7:JS模板 8:评论模板 99:自定义模板
 * @property int    $status
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 */
class Template extends BaseModel
{
    public const TYPE_HOME    = 1; // 1:首页模板
    public const TYPE_COVER   = 2; // 2:封面模板
    public const TYPE_LIST    = 3; // 3:列表模板
    public const TYPE_CONTENT = 4; // 4:内容模板
    public const TYPE_TAG     = 5; // 5:标签模板
    public const TYPE_PUBLIC  = 6; // 6:公共模板
    public const TYPE_JS      = 7; // 7:JS模板
    public const TYPE_COMMENT = 8; // 8:评论模板
    public const TYPE_CUSTOM  = 99; // 99:自定义模板

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'template';
}
