<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\easy\common;

use app\easy\model\Channel;
use Illuminate\Database\Schema\Builder;
use PDO;
use support\Db;
use support\exception\BusinessException;

class Util
{
    /**
     * 表单类型到插件的映射
     *
     * @return list<list<\string>>
     */
    public static function methodControlMap(): array
    {
        return [
            // method=>[控件]
            'integer' => ['InputNumber'],
            'string'  => ['Input'],
            'text'    => ['TextArea'],
            'date'    => ['DatePicker'],
            'enum'    => ['Select'],
            'float'   => ['Input'],

            'tinyInteger'   => ['InputNumber'],
            'smallInteger'  => ['InputNumber'],
            'mediumInteger' => ['InputNumber'],
            'bigInteger'    => ['InputNumber'],

            'unsignedInteger'       => ['InputNumber'],
            'unsignedTinyInteger'   => ['InputNumber'],
            'unsignedSmallInteger'  => ['InputNumber'],
            'unsignedMediumInteger' => ['InputNumber'],
            'unsignedBigInteger'    => ['InputNumber'],

            'decimal' => ['Input'],
            'double'  => ['Input'],

            'mediumText' => ['TextArea'],
            'longText'   => ['TextArea'],

            'dateTime' => ['DateTimePicker'],

            'time'      => ['DateTimePicker'],
            'timestamp' => ['DateTimePicker'],

            'char' => ['Input'],

            'binary' => ['Input'],

            'json' => ['input'],
        ];
    }

    /**
     * 数据库类型到插件的转换
     *
     * @param mixed $type
     */
    public static function typeToControl($type): string
    {
        if (stripos($type, 'int') !== false) {
            return 'inputNumber';
        }
        if (stripos($type, 'time') !== false || stripos($type, 'date') !== false) {
            return 'dateTimePicker';
        }
        if (stripos($type, 'text') !== false) {
            return 'textArea';
        }
        if ($type === 'enum') {
            return 'select';
        }

        return 'input';
    }

    /**
     * 数据库类型到表单类型的转换
     *
     * @param mixed $type
     * @param mixed $unsigned
     *
     * @return string
     */
    public static function typeToMethod($type, $unsigned = false)
    {
        if (stripos($type, 'int') !== false) {
            $type = str_replace('int', 'Integer', $type);

            return $unsigned ? 'unsigned' . ucfirst($type) : lcfirst($type);
        }
        $map = [
            'int'        => 'integer',
            'varchar'    => 'string',
            'mediumtext' => 'mediumText',
            'longtext'   => 'longText',
            'datetime'   => 'dateTime',
        ];

        return $map[$type] ?? $type;
    }

    /**
     * 按表获取摘要
     *
     * @param null  $section
     * @param mixed $table
     *
     * @return array|mixed
     *
     * @throws BusinessException
     */
    public static function getSchema($table, $section = null)
    {
        $database   = config('database.connections')['mysql']['database'];
        $prefix     = config('database.connections')['mysql']['prefix'];
        $schema_raw = $section !== 'table' ? Db::select("select * from information_schema.COLUMNS where TABLE_SCHEMA = '{$database}' and table_name = '{$prefix}{$table}' order by ORDINAL_POSITION") : [];

        $forms   = [];
        $columns = [];

        foreach ($schema_raw as $item) {
            $field   = $item->COLUMN_NAME;
            $default = null;
            if ($item->COLUMN_DEFAULT !== null && strtoupper($item->COLUMN_DEFAULT) !== 'NULL') {
                $default = trim($item->COLUMN_DEFAULT, "'");
            }
            $columns[$field] = [
                'field'          => $field,
                'type'           => Util::typeToMethod($item->DATA_TYPE, (bool) strpos($item->COLUMN_TYPE, 'unsigned')),
                'comment'        => $item->COLUMN_COMMENT,
                'default'        => $default,
                'length'         => static::getLengthValue($item),
                'nullable'       => $item->IS_NULLABLE !== 'NO',
                'primary_key'    => $item->COLUMN_KEY === 'PRI',
                'auto_increment' => str_contains($item->EXTRA, 'auto_increment'),
            ];

            $forms[$field] = [
                'field'        => $field,
                'comment'      => $item->COLUMN_COMMENT,
                'control'      => static::typeToControl($item->DATA_TYPE),
                'form_show'    => $item->COLUMN_KEY !== 'PRI',
                'list_show'    => true,
                'enable_sort'  => false,
                'searchable'   => false,
                'search_type'  => 'normal',
                'control_args' => '',
            ];
        }
        $table_schema = $section === 'table' || ! $section ? Db::select("SELECT TABLE_COMMENT FROM  information_schema.`TABLES` WHERE  TABLE_SCHEMA='{$database}' and TABLE_NAME='{$prefix}{$table}'") : [];
        $indexes      = ! $section           || in_array($section, ['keys', 'table'], true) ? Db::select("SHOW INDEX FROM `{$prefix}{$table}`") : [];
        $keys         = [];
        $primary_key  = [];

        foreach ($indexes as $index) {
            $key_name = $index->Key_name;
            if ($key_name === 'PRIMARY') {
                $primary_key[] = $index->Column_name;

                continue;
            }
            if (! isset($keys[$key_name])) {
                $keys[$key_name] = [
                    'name'    => $key_name,
                    'columns' => [],
                    'type'    => $index->Non_unique === 0 ? 'unique' : 'normal',
                ];
            }
            $keys[$key_name]['columns'][] = $index->Column_name;
        }

        $data = [
            'table'   => ['name' => $table, 'comment' => $table_schema[0]->TABLE_COMMENT ?? '', 'primary_key' => $primary_key],
            'columns' => $columns,
            'forms'   => $forms,
            'keys'    => array_reverse($keys, true),
        ];

        $schema          = Channel::where('table_name', "{$table}")->value('value');
        $form_schema_map = $schema ? json_decode($schema, true) : [];

        foreach ($data['forms'] as $field => $item) {
            if (isset($form_schema_map[$field])) {
                $data['forms'][$field] = $form_schema_map[$field];
            }
        }

        return $section ? $data[$section] : $data;
    }

    /**
     * 获取字段长度或默认值
     *
     * @param mixed $schema
     *
     * @return mixed|string
     */
    public static function getLengthValue($schema)
    {
        $type = $schema->DATA_TYPE;
        if (in_array($type, ['float', 'decimal', 'double'], true)) {
            return "{$schema->NUMERIC_PRECISION},{$schema->NUMERIC_SCALE}";
        }
        if ($type === 'enum') {
            return implode(',', array_map(static fn ($item) => trim($item, "'"), explode(',', substr($schema->COLUMN_TYPE, 5, -1))));
        }
        if (in_array($type, ['varchar', 'text', 'char'], true)) {
            return $schema->CHARACTER_MAXIMUM_LENGTH;
        }
        if (in_array($type, ['time', 'datetime', 'timestamp'], true)) {
            return $schema->CHARACTER_MAXIMUM_LENGTH;
        }

        return '';
    }

    /**
     * 数据库字符串转义
     *
     * @param mixed $var
     *
     * @return false|string
     */
    public static function pdoQuote($var)
    {
        return Db::connection()->getPdo()->quote($var, PDO::PARAM_STR);
    }

    /**
     * 变量或数组中的元素只能是字母数字下划线组合
     *
     * @param mixed $var
     *
     * @return mixed
     *
     * @throws BusinessException
     */
    public static function filterAlphaNum($var)
    {
        $vars = (array) $var;
        array_walk_recursive($vars, static function ($item) {
            if (is_string($item) && ! preg_match('/^[a-zA-Z_0-9]+$/', $item)) {
                throw new BusinessException('参数不合法');
            }
        });

        return $var;
    }

    /**
     * 获取SchemaBuilder
     */
    public static function schema(): Builder
    {
        return Db::schema('mysql');
    }

    /**
     * 转换为驼峰
     */
    public static function camel(string $value): string
    {
        static $cache = [];
        $key          = $value;

        if (isset($cache[$key])) {
            return $cache[$key];
        }

        $value = ucwords(str_replace(['-', '_'], ' ', $value));

        return $cache[$key] = str_replace(' ', '', $value);
    }

    /**
     * 转换为小驼峰
     *
     * @param mixed $value
     */
    public static function smCamel($value): string
    {
        return lcfirst(static::camel($value));
    }
}
