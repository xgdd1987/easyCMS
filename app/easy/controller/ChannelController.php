<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\easy\controller;

use app\easy\common\Util;
use app\easy\model\Channel;
use app\easy\validate\ChannelValidate;
use app\model\BaseModel;
use Illuminate\Database\Schema\Blueprint;
use support\Db;
use support\exception\BusinessException;
use support\Request;
use support\Response;
use Throwable;

class ChannelController
{
    /**
     * 模型列表
     */
    #[permits('easy:channel:list')]
    public function index(Request $request): Response
    {
        return view('channel/channelList');
    }

    /**
     * 模型列表
     */
    #[permits('easy:channel:list')]
    public function channelListData(Request $request): Response
    {
        $list = Channel::orderBy('id', 'ASC')->get()->toArray();

        return json(['code' => 0, 'msg' => '', 'count' => 0, 'data' => $list]);
    }

    /**
     * 模型增加
     */
    #[permits('easy:channel:add')]
    public function channelAdd(Request $request): Response
    {
        $data['channel'] = new Channel();

        return view('channel/channelAdd', $data);
    }

    /**
     * 模型增加
     */
    #[permits('easy:channel:add')]
    public function doChannelAdd(Request $request): Response
    {
        $code = 0;
        $msg  = '';

        $postData = $request->post();

        $validate = new ChannelValidate();
        if ($validate->check($postData)) {
            $channel             = new Channel();
            $channel->name       = $postData['name'];
            $channel->table_name = Channel::TABLE_PREFIX . $postData['table_name'];
            $channel->status     = BaseModel::STATUS_NORMAL;
            // 创建table
            $tableName = $channel->table_name;
            if (Db::schema()->hasTable($tableName)) {
                $code = 1;
                $msg  = '该Table名称已经存在';
            } else {
                $columns = $postData['columns'];
                $forms   = $postData['forms'];
                $keys    = $postData['keys'];

                $primary_key_count = 0;

                foreach ($columns as $index => $item) {
                    $columns[$index]['field'] = trim($item['field']);
                    if (! $item['field']) {
                        unset($columns[$index]);

                        continue;
                    }
                    $columns[$index]['primary_key'] = ! empty($item['primary_key']);
                    if ($columns[$index]['primary_key']) {
                        $primary_key_count++;
                    }
                    $columns[$index]['auto_increment'] = ! empty($item['auto_increment']);
                    $columns[$index]['nullable']       = ! empty($item['nullable']);
                    if ($item['default'] === '') {
                        $columns[$index]['default'] = null;
                    } elseif ($item['default'] === "''") {
                        $columns[$index]['default'] = '';
                    }
                }

                if ($primary_key_count > 1) {
                    $code = 1;
                    $msg  = '不支持复合主键';
                } else {
                    foreach ($forms as $index => $item) {
                        if (! $item['field']) {
                            unset($forms[$index]);

                            continue;
                        }
                        $forms[$index]['form_show']   = ! empty($item['form_show']);
                        $forms[$index]['list_show']   = ! empty($item['list_show']);
                        $forms[$index]['enable_sort'] = ! empty($item['enable_sort']);
                        $forms[$index]['searchable']  = ! empty($item['searchable']);
                    }

                    foreach ($keys as $index => $item) {
                        if (! $item['name'] || ! $item['columns']) {
                            unset($keys[$index]);
                        }
                    }
                    // 创建table
                    Db::schema()->create($channel->table_name, function (Blueprint $table) use ($columns) {
                        $type_method_map = Util::methodControlMap();

                        foreach ($columns as $column) {
                            if (! isset($column['type'])) {
                                throw new BusinessException("请为{$column['field']}选择类型");
                            }
                            if (! isset($type_method_map[$column['type']])) {
                                throw new BusinessException("不支持的类型{$column['type']}");
                            }
                            $this->createColumn($column, $table);
                        }
                        $table->charset   = 'utf8mb4';
                        $table->collation = 'utf8mb4_general_ci';
                        $table->engine    = 'InnoDB';
                    });
                    // 索引
                    Db::schema()->table($channel->table_name, static function (Blueprint $table) use ($keys) {
                        foreach ($keys as $key) {
                            $name    = $key['name'];
                            $columns = is_array($key['columns']) ? $key['columns'] : explode(',', $key['columns']);
                            $type    = $key['type'];
                            if ($type === 'unique') {
                                $table->unique($columns, $name);

                                continue;
                            }
                            $table->index($columns, $name);
                        }
                    });

                    $form_schema_map = [];

                    foreach ($forms as $item) {
                        $form_schema_map[$item['field']] = $item;
                    }
                    $form_schema_map = json_encode($form_schema_map, JSON_UNESCAPED_UNICODE);
                    $channel->value  = $form_schema_map;
                    if ($channel->save()) {
                        $msg = '创建成功';
                        // 创建model
                        $this->createModel($channel);
                    } else {
                        $code = 1;
                        $msg  = '创建失败';
                    }
                }
            }
        } else {
            $code = 1;
            $msg  = $validate->getError();
        }

        return json(['code' => $code, 'msg' => $msg]);
    }

    /**
     * 模型修改
     */
    #[permits('easy:channel:edit')]
    public function channelEdit(Request $request): Response
    {
        $id              = $request->get('id');
        $data['channel'] = Channel::find($id);

        return view('channel/channelEdit', $data);
    }

    /**
     * 模型修改
     */
    #[permits('easy:channel:edit')]
    public function doChannelEdit(Request $request): Response
    {
        $code = 0;
        $msg  = '';

        $postData = $request->post();

        $validate = new ChannelValidate();
        if ($validate->scene('edit')->check($postData)) {
            $channel       = Channel::find($postData['id']);
            $channel->name = $postData['name'];
            if ($channel === null || $channel->id < 1) {
                $code = 1;
                $msg  = '该模型不存在';
            } else {
                $table_name            = $channel->table_name;
                $columns               = $postData['columns'];
                $forms                 = $postData['forms'];
                $keys                  = $postData['keys'];
                $primary_key           = null;
                $auto_increment_column = null;
                $schema                = Util::getSchema($table_name);
                $old_columns           = $schema['columns'];
                $old_primary_key       = $schema['table']['primary_key'][0] ?? null;

                $primary_key_count = $auto_increment_count = 0;

                foreach ($columns as $index => $item) {
                    $columns[$index]['field'] = trim($item['field']);
                    if (! $item['field']) {
                        unset($columns[$index]);

                        continue;
                    }
                    $field                             = $item['field'];
                    $columns[$index]['auto_increment'] = ! empty($item['auto_increment']);
                    $columns[$index]['nullable']       = ! empty($item['nullable']);
                    $columns[$index]['primary_key']    = ! empty($item['primary_key']);
                    if ($columns[$index]['primary_key']) {
                        $primary_key                 = $item['field'];
                        $columns[$index]['nullable'] = false;
                        $primary_key_count++;
                    }
                    if ($item['default'] === '') {
                        $columns[$index]['default'] = null;
                    } elseif ($item['default'] === "''") {
                        $columns[$index]['default'] = '';
                    }
                    if ($columns[$index]['auto_increment']) {
                        $auto_increment_count++;
                        if (! isset($old_columns[$field]) || ! $old_columns[$field]['auto_increment']) {
                            $auto_increment_column = $columns[$index];
                            unset($auto_increment_column['old_field']);
                            $columns[$index]['auto_increment'] = false;
                        }
                    }
                }

                if ($primary_key_count > 1) {
                    $code = 1;
                    $msg  = '不支持复合主键';
                } elseif ($auto_increment_count > 1) {
                    $code = 1;
                    $msg  = '一个表只能有一个自增字段，并且必须为key';
                } else {
                    foreach ($forms as $index => $item) {
                        if (! $item['field']) {
                            unset($forms[$index]);

                            continue;
                        }
                        $forms[$index]['form_show']   = ! empty($item['form_show']);
                        $forms[$index]['list_show']   = ! empty($item['list_show']);
                        $forms[$index]['enable_sort'] = ! empty($item['enable_sort']);
                        $forms[$index]['searchable']  = ! empty($item['searchable']);
                    }

                    foreach ($keys as $index => $item) {
                        if (! $item['name'] || ! $item['columns']) {
                            unset($keys[$index]);
                        }
                    }

                    $type_method_map = Util::methodControlMap();

                    foreach ($columns as $column) {
                        if (! isset($type_method_map[$column['type']])) {
                            throw new BusinessException("不支持的类型{$column['type']}");
                        }
                        $field      = $column['old_field'] ?? $column['field'];
                        $old_column = $old_columns[$field] ?? [];

                        // 类型更改
                        foreach ($old_column as $key => $value) {
                            if (array_key_exists($key, $column) && ($column[$key] !== $value || ($key === 'default' && $column[$key] !== $value))) {
                                $this->modifyColumn($column, $table_name);
                                break;
                            }
                        }
                    }

                    $table = Util::getSchema($table_name, 'table');

                    $old_columns = Util::getSchema($table_name, 'columns');
                    Db::schema()->table($table_name, function (Blueprint $table) use ($columns, $old_columns, $keys) {
                        foreach ($columns as $column) {
                            $field = $column['field'];
                            // 新字段
                            if (! isset($old_columns[$field])) {
                                $this->createColumn($column, $table);
                            }
                        }

                        // 更新索引名字
                        foreach ($keys as $key) {
                            if (! empty($key['old_name']) && $key['old_name'] !== $key['name']) {
                                $table->renameIndex($key['old_name'], $key['name']);
                            }
                        }
                    });
                    $prefix = config('database.connections')['mysql']['prefix'];
                    // 找到删除的字段
                    $old_columns         = Util::getSchema($table_name, 'columns');
                    $exists_column_names = array_column($columns, 'field', 'field');
                    $old_columns_names   = array_column($old_columns, 'field');
                    $drop_column_names   = array_diff($old_columns_names, $exists_column_names);
                    $drop_column_names   = Util::filterAlphaNum($drop_column_names);

                    foreach ($drop_column_names as $drop_column_name) {
                        Db::statement("ALTER TABLE {$prefix}{$table_name} DROP COLUMN `{$drop_column_name}`");
                    }

                    $old_keys = Util::getSchema($table_name, 'keys');
                    Util::schema()->table($table_name, static function (Blueprint $table) use ($keys, $old_keys) {
                        foreach ($keys as $key) {
                            $key_name = $key['name'];
                            $old_key  = $old_keys[$key_name] ?? [];
                            // 如果索引有变动，则删除索引，重新建立索引
                            if ($old_key && ($key['type'] !== $old_key['type'] || $key['columns'] !== implode(',', $old_key['columns']))) {
                                $old_key = [];
                                unset($old_keys[$key_name]);
                                echo "Drop Index {$key_name}\n";
                                $table->dropIndex($key_name);
                            }
                            // 重新建立索引
                            if (! $old_key) {
                                $name    = $key['name'];
                                $columns = is_array($key['columns']) ? $key['columns'] : explode(',', $key['columns']);
                                $type    = $key['type'];
                                if ($type === 'unique') {
                                    $table->unique($columns, $name);

                                    continue;
                                }
                                echo "Create Index {$key_name}\n";
                                $table->index($columns, $name);
                            }
                        }

                        // 找到删除的索引
                        $exists_key_names = array_column($keys, 'name', 'name');
                        $old_keys_names   = array_column($old_keys, 'name');
                        $drop_keys_names  = array_diff($old_keys_names, $exists_key_names);

                        foreach ($drop_keys_names as $name) {
                            echo "Drop Index {$name}\n";
                            $table->dropIndex($name);
                        }
                    });

                    // 变更主键
                    if ($old_primary_key !== $primary_key) {
                        if ($old_primary_key) {
                            Db::statement("ALTER TABLE `{$table_name}` DROP PRIMARY KEY");
                        }
                        if ($primary_key) {
                            $primary_key = Util::filterAlphaNum($primary_key);
                            Db::statement("ALTER TABLE `{$table_name}` ADD PRIMARY KEY(`{$primary_key}`)");
                        }
                    }

                    // 一个表只能有一个 auto_increment 字段，并且是key，所以需要在最后设置
                    if ($auto_increment_column) {
                        $this->modifyColumn($auto_increment_column, $table_name);
                    }

                    $form_schema_map = [];

                    foreach ($forms as $item) {
                        $form_schema_map[$item['field']] = $item;
                    }
                    $form_schema_map = json_encode($form_schema_map, JSON_UNESCAPED_UNICODE);
                    $channel->value  = $form_schema_map;
                    if ($channel->save()) {
                        $msg = '修改成功';
                        // 更新model
                        $this->createModel($channel);
                    } else {
                        $code = 1;
                        $msg  = '修改失败';
                    }
                }
            }
        } else {
            $code = 1;
            $msg  = $validate->getError();
        }

        return json(['code' => $code, 'msg' => $msg]);
    }

    /**
     * 模型删除
     * 这里需要做判断，是否关联删除相关数据。目前暂不提供删除功能
     */
    #[permits('easy:channel:delete')]
    private function channelDelete(Request $request): Response
    {
        $id      = $request->post('id');
        $channel = Channel::find($id);
        if ($channel === null || $channel->id < 1) {
            $code = 1;
            $msg  = '该模型不存在';
        } else {
            if ($channel->delete()) {
                $msg = '操作成功';
            } else {
                $code = 1;
                $msg  = '操作失败';
            }
        }

        return json(['code' => $code, 'msg' => $msg]);
    }

    /**
     * 创建目录
     *
     * @param mixed $file
     *
     * @return void
     */
    protected function mkdir($file)
    {
        $path = pathinfo($file, PATHINFO_DIRNAME);
        if (! is_dir($path)) {
            mkdir($path, 0777, true);
        }
    }

    /**
     * 创建字段
     *
     * @param mixed $column
     *
     * @return mixed
     */
    private function createColumn($column, Blueprint $table)
    {
        $method = $column['type'];
        $args   = [$column['field']];
        if (stripos($method, 'int') !== false) {
            // auto_increment 会自动成为主键
            if ($column['auto_increment']) {
                $column['nullable'] = false;
                $column['default']  = null;
                $args[]             = true;
            }
        } elseif (in_array($method, ['string', 'char'], true) || stripos($method, 'time') !== false) {
            if ($column['length']) {
                $args[] = $column['length'];
            }
        } elseif ($method === 'enum') {
            $args[] = array_map('trim', explode(',', $column['length']));
        } elseif (in_array($method, ['float', 'decimal', 'double'], true)) {
            if ($column['length']) {
                $args = array_merge($args, array_map('trim', explode(',', $column['length'])));
            }
        } else {
            $column['auto_increment'] = false;
        }

        $column_def = [$table, $method](...$args);
        if (! empty($column['comment'])) {
            $column_def = $column_def->comment($column['comment']);
        }

        if (! $column['auto_increment'] && $column['primary_key']) {
            $column_def = $column_def->primary(true);
        }

        if ($column['auto_increment'] && ! $column['primary_key']) {
            $column_def = $column_def->primary(false);
        }
        $column_def = $column_def->nullable($column['nullable']);

        if ($column['primary_key']) {
            $column_def = $column_def->nullable(false);
        }

        if ($method !== 'text' && $column['default'] !== null) {
            $column_def->default($column['default']);
        }

        return $column_def;
    }

    /**
     * 表摘要
     */
    #[permits('easy:channel:create', 'easy:channel:edit')]
    public function schema(Request $request): Response
    {
        $table = $request->get('table');
        $data  = Util::getSchema($table);

        return $this->json(0, 'ok', [
            'table'   => $data['table'],
            'columns' => array_values($data['columns']),
            'forms'   => array_values($data['forms']),
            'keys'    => array_values($data['keys']),
        ]);
    }

    /**
     * 更改字段
     *
     * @param mixed $column
     * @param mixed $table
     *
     * @return mixed
     *
     * @throws BusinessException
     */
    private function modifyColumn($column, $table)
    {
        $table          = Util::filterAlphaNum($table);
        $method         = Util::filterAlphaNum($column['type']);
        $field          = Util::filterAlphaNum($column['field']);
        $old_field      = Util::filterAlphaNum($column['old_field'] ?? null);
        $nullable       = $column['nullable'];
        $default        = $column['default'] !== null ? Util::pdoQuote($column['default']) : null;
        $comment        = Util::pdoQuote($column['comment']);
        $auto_increment = $column['auto_increment'];
        $length         = (int) $column['length'];

        if ($column['primary_key']) {
            $default = null;
        }
        $prefix = config('database.connections')['mysql']['prefix'];
        if ($old_field && $old_field !== $field) {
            $sql = "ALTER TABLE `{$prefix}{$table}` CHANGE COLUMN `{$old_field}` `{$field}` ";
        } else {
            $sql = "ALTER TABLE `{$prefix}{$table}` MODIFY `{$field}` ";
        }

        if (stripos($method, 'integer') !== false) {
            $type = str_ireplace('integer', 'int', $method);
            if (stripos($method, 'unsigned') !== false) {
                $type = str_ireplace('unsigned', '', $type);
                $sql .= "{$type} ";
                $sql .= 'unsigned ';
            } else {
                $sql .= "{$type} ";
            }
            if ($auto_increment) {
                $column['nullable'] = false;
                $column['default']  = null;
                $sql .= 'AUTO_INCREMENT ';
            }
        } else {
            switch ($method) {
                case 'string':
                    $length = $length ?: 255;
                    $sql .= "varchar({$length}) ";
                    break;

                case 'char':
                case 'time':
                    $sql .= $length ? "{$method}({$length}) " : "{$method} ";
                    break;

                case 'enum':
                    $args = array_map('trim', explode(',', (string) $column['length']));

                    foreach ($args as $key => $value) {
                        $args[$key] = Util::pdoQuote($value);
                    }
                    $sql .= 'enum(' . implode(',', $args) . ') ';
                    break;

                case 'double':
                case 'float':
                case 'decimal':
                    if (trim($column['length'])) {
                        $args = array_map('intval', explode(',', $column['length']));
                        $args[1] ??= $args[0];
                        $sql .= "{$method}({$args[0]}, {$args[1]}) ";
                        break;
                    }
                    $sql .= "{$method} ";
                    break;

                default:
                    $sql .= "{$method} ";
            }
        }

        if (! $nullable) {
            $sql .= 'NOT NULL ';
        }

        if ($method !== 'text' && $default !== null) {
            $sql .= "DEFAULT {$default} ";
        }

        if ($comment !== null) {
            $sql .= "COMMENT {$comment} ";
        }

        //        echo "{$sql}\n";
        Db::statement($sql);
    }

    /**
     * 创建model，会覆盖已存在的model。
     *
     * @return void
     */
    private function createModel(Channel $channel)
    {
        $namespace = 'app\model';
        $class     = Util::camel($channel->table_name);
        $file      = BASE_PATH . '\\' . $namespace . '\\';
        $table     = $channel->table_name;

        $this->mkdir($file);
        $table_val  = "'{$table}'";
        $pk         = 'id';
        $properties = '';
        $timestamps = '';
        $columns    = [];

        try {
            $database = config('database.connections')['mysql']['database'];
            $prefix   = config('database.connections')['mysql']['prefix'];

            foreach (Db::select("select COLUMN_NAME,DATA_TYPE,COLUMN_KEY,COLUMN_COMMENT from INFORMATION_SCHEMA.COLUMNS where table_name = '{$prefix}{$table}' and table_schema = '{$database}' order by ORDINAL_POSITION") as $item) {
                if ($item->COLUMN_KEY === 'PRI') {
                    $pk = $item->COLUMN_NAME;
                    $item->COLUMN_COMMENT .= '(主键)';
                }
                $type = $this->getType($item->DATA_TYPE);
                $properties .= " * @property {$type} \${$item->COLUMN_NAME} {$item->COLUMN_COMMENT}\n";
                $columns[$item->COLUMN_NAME] = $item->COLUMN_NAME;
            }
        } catch (Throwable $e) {
            echo $e;
        }
        if (! isset($columns['created_at']) || ! isset($columns['updated_at'])) {
            $timestamps = <<<'EOF'
                /**
                     * Indicates if the model should be timestamped.
                     *
                     * @var bool
                     */
                    public $timestamps = false;
                EOF;
        }
        $properties    = rtrim($properties) ?: ' *';
        $model_content = <<<EOF
            <?php

            namespace {$namespace};

            use app\\admin\\model\\BaseModel;

            /**
            {$properties}
             */
            class {$class} extends BaseModel
            {
                /**
                 * The table associated with the model.
                 *
                 * @var string
                 */
                protected \$table = {$table_val};

                /**
                 * The primary key associated with the table.
                 *
                 * @var string
                 */
                protected \$primaryKey = '{$pk}';

                {$timestamps}


            }

            EOF;
        file_put_contents($file . $class . '.php', $model_content);
    }

    /**
     * 字段类型到php类型映射
     */
    private function getType(string $type): string
    {
        if (str_contains($type, 'int')) {
            return 'integer';
        }

        switch ($type) {
            case 'varchar':
            case 'string':
            case 'text':
            case 'date':
            case 'time':
            case 'guid':
            case 'datetimetz':
            case 'datetime':
            case 'decimal':
            case 'enum':
                return 'string';

            case 'boolean':
                return 'integer';

            case 'float':
                return 'float';

            default:
                return 'mixed';
        }
    }

    /**
     * 返回格式化json数据
     */
    private function json(int $code, string $msg = 'ok', array $data = []): Response
    {
        return json(['code' => $code, 'data' => $data, 'msg' => $msg]);
    }
}
