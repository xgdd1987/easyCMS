<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\easy\controller;

use app\easy\common\FileSystem;
use app\easy\model\Theme;
use app\model\BaseModel;
use support\Request;
use support\Response;

class ThemeController
{
    #[permits('easy:theme:list')]
    public function index(Request $request): Response
    {
        $list = Theme::select(['id', 'name', 'desc', 'pic', 'type', 'price', 'author', 'author_home', 'version', 'is_activate'])->where('status', BaseModel::STATUS_NORMAL)->get()->toArray();

        return view('theme/list', ['list' => $list]);
    }

    #[permits('easy:theme:edit')]
    public function themeEdit(Request $request): Response
    {
        $id       = $request->get('id');
        $fileType = $request->get('fileType');

        $fileType         = $fileType === null ? 'template' : $fileType;
        $theme            = Theme::find($id);
        $data['theme']    = $theme;
        $data['fileType'] = $fileType;
        if ($theme !== null && $theme->id > 0) {
            $templateHome = base_path();
            if ((int) ($fileType === 'template')) {
                $templateHome .= Theme::TEMPLATE_ROOT;
            } else {
                $templateHome .= Theme::STATIC_FILE_ROOT;
            }

            $fileSystem    = new FileSystem();
            $files         = $fileSystem->directory_map($templateHome);
            $files         = $this->array2TreeData($files);
            $data['files'] = $files;

            return view('theme/templateEdit', $data);
        }

        return response('error.');
    }

    private function arraySort($data): array
    {
        $newArray = [];

        foreach ($data as $k => $v) {
            $tempA = [];
            $tempB = [];
            if (is_numeric($k)) {
                $tempB[] = $v;
            } else {
                $tempA[$k] = $this->arraySort($v);
            }
            $newArray[] = array_merge($tempA, $tempB);
        }

        return $newArray;
    }

    private function array2TreeData($data, $path = DIRECTORY_SEPARATOR): array
    {
        $treeData = [];

        foreach ($data as $k => $v) {
            if (is_array($v)) {
                $tempPath         = $path . $k;
                $temp             = [];
                $temp['title']    = '<i class="fa fa-folder"></i>  ' . substr($k, 0, -1);
                $temp['children'] = $this->array2TreeData($v, $tempPath);
                $temp['path']     = $tempPath;
                $temp['type']     = 1; // 是否文件夹 1：是 0：否
                $temp['spread']   = true;
                $treeData[]       = $temp;
            } else {
                $temp          = [];
                $temp['title'] = $v;
                $temp['path']  = $path;
                $temp['type']  = 0; // 是否文件夹 1：是 0：否
                $treeData[]    = $temp;
            }
        }

        return $treeData;
    }

    public function getFileContent(Request $request): Response
    {
        $path     = $request->post('path');
        $fileName = $request->post('fileName');
        $fileType = $request->post('fileType'); // 文件所在的目录

        $content = '';
        $data    = [];
        if (str_ends_with($fileName, '.html') || str_ends_with($fileName, '.js') || str_ends_with($fileName, '.json') || str_ends_with($fileName, '.css')) {
            $fileHome = '';
            if ($fileType === 'template') {
                $fileHome = Theme::TEMPLATE_ROOT;
            } else {
                $fileHome = Theme::STATIC_FILE_ROOT;
            }
            $fileFullPath     = base_path() . $fileHome . $path . $fileName;
            $content          = file_get_contents($fileFullPath);
            $data['code']     = $content;
            $data['fileName'] = $fileName;
            $data['filePath'] = $path;
        }

        return json(['code' => 0, 'msg' => '', 'data' => $data]);
    }

    #[permits('easy:theme:edit')]
    public function saveFile(Request $request): Response
    {
        $code        = 0;
        $msg         = '';
        $oldFileName = $request->post('oldFileName');
        $fileName    = $request->post('fileName');
        $oldFilePath = $request->post('oldFilePath');
        $filePath    = $request->post('filePath');
        $fileCode    = $request->post('fileCode');
        $isNewFile   = $request->post('isNewFile'); // 是否新建文件
        $fileType    = $request->post('fileType'); // 文件所在的目录

        if ((int) $isNewFile === 1) {
            $isNewFile = true;
        } else {
            $isNewFile = false;
        }
        if (! str_ends_with($fileName, '.html') && ! str_ends_with($fileName, '.js') && ! str_ends_with($fileName, '.json') && ! str_ends_with($fileName, '.css')) {
            $code = 1;
            $msg  = '只允许创建html、js、json、css类型的文件';
        } else {
            $fileHome = '';
            if ($fileType === 'template') {
                $fileHome = Theme::TEMPLATE_ROOT;
            } else {
                $fileHome = Theme::STATIC_FILE_ROOT;
            }
            $oldFileFullPath = base_path() . $fileHome . $oldFilePath;
            $fileFullPath    = base_path() . $fileHome . $filePath;

            $fileSystem = new FileSystem();
            if (! is_dir($fileFullPath)) {
                makeDir($fileFullPath);
            }
            if ($fileSystem->write_file($fileFullPath . $fileName, $fileCode)) {
                $msg = '保存成功';
                if ($isNewFile === false && $oldFilePath . $oldFileName !== $filePath . $fileName) {
                    // 删除原来的文件
                    if (is_file($oldFileFullPath . $oldFileName)) {
                        unlink($oldFileFullPath . $oldFileName);
                    }
                }
            } else {
                $code = 1;
                $msg  = '保存失败';
            }
        }

        return json(['code' => $code, 'msg' => $msg]);
    }

    #[permits('easy:theme:edit')]
    public function deleteFile(Request $request): Response
    {
        $code = 0;
        $msg  = '';

        $fileName = $request->post('fileName');
        $filePath = $request->post('filePath');
        $fileType = $request->post('fileType'); // 文件所在的目录

        $fileHome = '';
        if ($fileType === 'template') {
            $fileHome = Theme::TEMPLATE_ROOT;
        } else {
            $fileHome = Theme::STATIC_FILE_ROOT;
        }
        $fileFullPath = base_path() . $fileHome . $filePath;
        if ($fileName !== '' && file_exists($fileFullPath . $fileName)) {
            if (unlink($fileFullPath . $fileName)) {
                $msg = '删除成功';
            } else {
                $code = 1;
                $msg  = '删除失败';
            }
        } else {
            $code = 1;
            $msg  = '文件不存在';
        }

        return json(['code' => $code, 'msg' => $msg]);
    }

    public function getFileJson(Request $request): Response
    {
        $code     = 1;
        $msg      = '';
        $data     = [];
        $id       = $request->post('id');
        $fileType = $request->post('fileType'); // 文件所在的目录

        $theme = Theme::find($id);
        if ($theme !== null && $theme->id > 0) {
            $fileHome = '';
            if ($fileType === 'template') {
                $fileHome = Theme::TEMPLATE_ROOT;
            } else {
                $fileHome = Theme::STATIC_FILE_ROOT;
            }
            $templateHome  = base_path() . $fileHome;
            $fileSystem    = new FileSystem();
            $files         = $fileSystem->directory_map($templateHome);
            $files         = $this->array2TreeData($files);
            $data['files'] = $files;
        }

        return json(['code' => $code, 'msg' => $msg, 'data' => $data]);
    }
}
