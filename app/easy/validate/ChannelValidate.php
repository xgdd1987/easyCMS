<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\easy\validate;

use think\Validate;

class ChannelValidate extends Validate
{
    protected $rule = [
        'name'       => 'require|max:20',
        'table_name' => 'require|max:20|alphaDash',
    ];
    protected $message = [
        'name.require'         => '模型名称不能为空',
        'name.max'             => '模型名称不能超过20个字符',
        'table_name.require'   => 'Table名称不能为空',
        'table_name.max'       => 'Table名称不能超过20个字符',
        'table_name.alphaDash' => 'Table名称只能为字母、数字和_',
    ];
    protected $scene = [
        'edit' => ['name'],
    ];
}
