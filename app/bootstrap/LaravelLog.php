<?php

declare(strict_types=1);

/**
 * This file is part of easyCMS.
 *
 * (c) 2024 easyCMS <easyCMS@easycms.net.cn>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace app\bootstrap;

use Illuminate\Support\Str;
use support\Db;
use support\Log; // 新增
use Webman\Bootstrap;

// 新增

class LaravelLog implements Bootstrap
{
    public static function start($worker)
    {
        Db::listen(static function ($query) {
            $sql      = $query->sql;
            $bindings = [];
            if ($query->bindings) {
                foreach ($query->bindings as $v) {
                    if (is_numeric($v)) {
                        $bindings[] = $v;
                    } else {
                        $bindings[] = '"' . (string) $v . '"';
                    }
                }
            }
            $execute = Str::replaceArray('?', $bindings, $sql);
            if (trim($execute) !== 'select 1') {
                //            Log::debug($execute);
                print_r($execute . '    ' . $query->time . " ms\r\n");
            }
        });
    }
}
